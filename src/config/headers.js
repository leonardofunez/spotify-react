const spotifyToken = localStorage.getItem("spotifyToken")
export const Headers = {
    Accept: "application/json",
    Authorization: "Bearer " + spotifyToken,
    "Content-Type": "application/json"
}