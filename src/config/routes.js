import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'

// Globals
import TopBar from '../components/TopBar/TopBar'
import MenuBar from '../components/MenuBar/MenuBar'
import Player from '../components/Player/Player'

// Auth
import Login from '../pages/Login/Login'

// Pages
import Browse from '../pages/Browse/Browse'
import Album from '../pages/Album/Album'
import AlbumsSaved from '../pages/SavedAlbums/SavedAlbums'
import TracksSaved from '../pages/SavedTracks/SavedTracks'
import ArtistsSaved from '../pages/SavedArtists/SavedArtists'
import Artist from '../pages/Artist/Artist'
import Playlist from '../pages/Playlist/Playlist'
import User from '../pages/User/User'

const routes = (
    <Router>
        <React.Fragment>
            <Route path='/login' component={Login} exact/>
            
            <Route path='/dashboard' component={TopBar}/>
            <Route path='/dashboard' component={MenuBar}/>
            <Route path='/dashboard' component={Player}/>

            <main className="app-container relative w-100 pl-40 pr-40 pt-100 pb-170">
                <Route path='/' component={Browse} exact/>
                <Route path='/dashboard' component={Browse} exact/>
                <Route path='/dashboard/browse' component={Browse} exact/>
                <Route path='/dashboard/albums/:id' component={Album} exact/>
                <Route path='/dashboard/saved-albums' component={AlbumsSaved} exact/>
                <Route path='/dashboard/saved-tracks' component={TracksSaved} exact/>
                <Route path='/dashboard/saved-artists' component={ArtistsSaved} exact/>
                <Route path='/dashboard/artists/:id' component={Artist} exact/>
                <Route path='/dashboard/playlists/:id' component={Playlist} exact/>
                <Route path='/dashboard/users/:id' component={User} exact/>
            </main>
        </React.Fragment>
    </Router>
)

export default routes;