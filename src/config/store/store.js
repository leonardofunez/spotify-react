import { createStore } from 'redux'

const reducer = (state, action) => {
    switch (action.type){
        case 'PLAY_TRACK':
            return {
                ...state, track: action.track
            }
        default:
            return state
    }

    // if(action.type === "PLAY_TRACK"){
    //     return {
    //         ...state, track: action.track
    //     }
    // }
    // return state
}

export default createStore(reducer, {track: []});