import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { Headers } from '../../config/headers'

export default class TopBar extends Component{
    state = {
        headers: Headers,
        current_user: [],
        current_user_photo: '',

        search_box: false,
        search_artists: [],
        search_albums: [],
        search_artists_switch: true,
        search_albums_switch: false,
    }

    componentDidMount(){
        this.getCurrentUser()
    }

    getCurrentUser(){
        fetch("https://api.spotify.com/v1/me", { headers: this.state.headers })
        .then( response => response.json())
        .then( current_user  => {
            this.setState({
                current_user: current_user,
                current_user_photo: current_user.images[0].url
            })
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    tabSearchButton(type){
        if( type === 'artists'){
            this.setState({
                search_artists_switch: true,
                search_albums_switch: false,
            })
        }else{
            this.setState({
                search_artists_switch: false,
                search_albums_switch: true,
            })
        }
    }

    searching(value){
        let search_value = value.target.value
        if(search_value !== ''){
            fetch(`https://api.spotify.com/v1/search?q=${search_value}&type=artist%2Calbum`, { headers: this.state.headers })
            .then( response => response.json())
            .then( search_result  => {
                if(search_result.artists.length > 0 || search_result.albums.items.length > 0){
                    this.setState({
                        search_box: true,
                        search_artists: search_result.artists.items,
                        search_albums: search_result.albums.items,
                    })
                }else{
                    this.searchingOff()
                }
            })
            .catch( error => 
                console.error('Error:', error)
            );
        }else{
            this.searchingOff()
        }
    }

    searchingOff = () => {
        this.setState({
            search_box: false, search_artists: []
        })
    }

    render(){
        const styleUser = {
            background: 'url(' + this.state.current_user_photo + ') no-repeat center 10% / cover'
        }

        return(
            <section className="top-bar pl-40 pr-40 pt-20 pb-20 fixed pos-top pos-right d-flex flex-between z-index-3">
                <div className="overlay overlay--top"></div>
                <div className="d-flex z-index-1">
                    <div className="nav-buttons-container d-flex mr-30">
                        <div className="nav-button nav-button--prev h-30-p w-30-p c-pointer d-block"></div>
                        <div className="nav-button nav-button--next h-30-p w-30-p c-pointer d-block"></div>
                    </div>
                    <div className="search-container relative">
                        <input className="input input--search input--bg-blue-dark" type="search" placeholder="Search" onChange={(e) => this.searching(e)} />
            
                        {/* Searchbox */}
                            <div className={this.state.search_box ? "search-results-box min-w-300 active" : "search-results-box min-w-300"} onMouseLeave={this.searchingOff}>
                                {/* Tabs header */}
                                    <div className="tabs-header d-flex">
                                        <div onClick={() => this.tabSearchButton('artists')} className={this.state.search_artists_switch ? "pl-20 pt-15 pb-15 font-14 font-weight-500 c-pointer w-50 bg-white tab artists-tab-header active" : "pl-20 pt-15 pb-15 font-14 font-weight-500 c-pointer w-50 bg-white tab artists-tab-header"}>
                                            <span>{`Artist `+ this.state.search_artists.length}</span>
                                        </div>
                                        <div onClick={() => this.tabSearchButton('albums')} className={this.state.search_albums_switch ? "pl-20 pt-15 pb-15 font-14 font-weight-500 c-pointer w-50 bg-white tab albums-tab-header active" : "pl-20 pt-15 pb-15 font-14 font-weight-500 c-pointer w-50 bg-white tab albums-tab-header"}>
                                            <span>{`Albums `+ this.state.search_albums.length}</span>
                                        </div>
                                    </div>
                                {/* .Tabs header */}

                                {/* Tabs body */}
                                    <div className="tabs-body bg-white max-height-300 overflow-scroll">
                                        { this.state.search_artists_switch ?
                                            <div className="tab-body-block">
                                                {   this.state.search_artists.length ?
                                                        this.state.search_artists.map( (artist, index) => 
                                                            <NavLink to={`/dashboard/artists/${artist.id}`} key={index} onClick={this.searchingOff}>
                                                                <div className="item-result p-10 font-14 color-dark c-pointer font-weight-500 truncate">{artist.name}</div>
                                                            </NavLink>
                                                        )
                                                    :
                                                        <div className="item-result p-10 font-14 color-dark c-pointer font-weight-500 truncate">There is no results :(</div>
                                                }
                                            </div>
                                        : null }

                                        { this.state.search_albums_switch ?
                                            <div className="tab-body-block">
                                                {   this.state.search_albums.length ?
                                                        this.state.search_albums.map( (album, index) => 
                                                            <NavLink to={`/dashboard/albums/${album.id}`} key={index} onClick={this.searchingOff}>
                                                                <div className="item-result p-10 font-14 color-dark c-pointer font-weight-500 truncate">{album.name}</div>
                                                            </NavLink>
                                                        )
                                                    :
                                                        <div className="item-result p-10 font-14 color-dark c-pointer font-weight-500 truncate">There is no results :(</div>
                                                }
                                            </div>
                                        : null }
                                    </div>
                                {/* .Tabs body */}
                            </div>
                        {/* .Searchbox */}
                    </div>
                </div>

                <NavLink to={`/dashboard/users/${this.state.current_user.id}`} className="relative z-index-1">
                    <div className="profile-container c-pointer color-white font-14 d-flex flex-middle">
                        <span className="profile-name mr-10 font-weight-500">{this.state.current_user.id}</span>
                        <div className="photo-circle h-30-p w-30-p circled bg-white relative" style={styleUser}></div>
                    </div>
                </NavLink>
            </section>
        )
    }
}