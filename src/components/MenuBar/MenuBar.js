import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { Headers } from '../../config/headers'

export default class MenuBar extends Component {
    state = {
        headers: Headers,
        playlists: [],
    }
    
    componentDidMount(){
        this.getPlaylists()
    }

    getPlaylists(){
        fetch("https://api.spotify.com/v1/me/playlists", { headers: this.state.headers })
        .then( response => response.json())
        .then( playlists  => {
            if(playlists.error){
                console.log(playlists.error.status)
                if (playlists.error.status === 401) window.location.href = '/login'
            }else{
                this.setState({ playlists: playlists.items})
            }
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    render(){
        return(
            <React.Fragment>
                <section className="menu-bar p-60 w-120-p h-100 fixed bg-darker color-white box-sizing-initial relative">
                    <NavLink to="/dashboard/browse" className="logo d-block h-50-p w-100 mb-40"></NavLink>

                    <NavLink to="/dashboard/browse" className="browse font-weight-500 color-white no-decoration d-block mb-15 pl-30">Browse</NavLink>
                    <NavLink to="/dashboard/radio" className="radio font-weight-500 color-white no-decoration d-block mb-15 pl-30">Radio</NavLink>
                    
                    <div className="mb-40"></div>

                    <span className="d-block align-left font-12 font-weight-600 spacing-2 uppercase mb-20 ">Library</span>
                    <div className="align-left font-weight-500 font-14 c-pointer mb-15 truncate">
                        <NavLink to="/dashboard/saved-artists" className="align-left font-weight-500 font-14 c-pointer mb-15">Artists</NavLink>
                    </div>
                    <div className="align-left font-weight-500 font-14 c-pointer mb-15 truncate">
                        <NavLink to="/dashboard/saved-albums" className="align-left font-weight-500 font-14 c-pointer mb-15">Albums</NavLink>
                    </div>
                    <div className="align-left font-weight-500 font-14 c-pointer mb-15 truncate">
                        <NavLink to="/dashboard/saved-tracks" className="align-left font-weight-500 font-14 c-pointer mb-15">Tracks</NavLink>
                    </div>
                    
                    <div className="mb-40"></div>
                    
                    <span className="d-block align-left font-12 font-weight-600 spacing-2 uppercase mb-20 ">Playlists</span>
                    { this.state.playlists.map( (item, index) =>  
                        <div className="align-left font-weight-500 font-14 c-pointer mb-15 truncate" key={index}>
                            <NavLink className="color-white no-decoration" to={`/dashboard/playlists/${item.id}`}>{item.name}</NavLink>
                        </div>
                    )}
                </section>
            </React.Fragment>
        )
    }
}