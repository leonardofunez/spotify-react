import React, { Component } from 'react'
import { Headers } from '../../../config/headers'

export default class DeletePlaylist extends Component{
    state = {
        header: Headers,
        visible: false,
    }

    componentWillReceiveProps(props){
        this.setState({
            visible: props.visible
        })
    }

    closeModal(){
        this.props.closeDeletePlaylist()
    }

    render(){
        return(
            <section className={this.state.visible ? "active modal-container overlay overlay--full" : "modal-container overlay overlay--full"}>
                <div className="modal-content bg-white max-w-680 h-auto p-30 z-index-1 rounded">
                    <div className="modal-header d-flex flex-between w-100 mb-20">
                        <div className="modal-header font-weight-600 uppercase font-12 lh-17">Delete playlist</div>
                        <div className="close close--dark h-20-p w-20-p" onClick={() => this.closeModal()}></div>
                    </div>

                    <div className="modal-body">
                        <h2 className="m-0 mb-20 font-18">Do you really want to delete this playlist?</h2>
                        <div className="d-flex">
                            <button className="button button--red mr-20" onClick={() => this.closeModal()}>Delete</button>
                            <button className="button button--solid" onClick={() => this.closeModal()}>Cancel</button>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}