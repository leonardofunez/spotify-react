import React, { Component } from 'react'
import { Headers } from '../../../config/headers'

export default class AddPlaylist extends Component{
    state = {
        headers: Headers,
        visible: false,
        current_user_id: '',
        playlist_name: 'New Playlist',
        playlist_description: 'Give you playlist a catchy description.',
        playlist_public: false
    }

    componentWillReceiveProps(props){
        this.setState({
            visible: props.visible
        })
    }

    componentDidMount(){
        this.getCurrentUserId()
    }

    closeModal(){
        this.props.closeAddPlaylist()
    }

    getCurrentUserId(){
        fetch("https://api.spotify.com/v1/me", { headers: this.state.headers })
        .then( response => response.json())
        .then( current_user  => {
            this.setState({
                current_user_id: current_user.id
            })
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    createPlaylist(){
        fetch(`https://api.spotify.com/v1/users/${this.state.current_user_id}/playlists`, {
            method: 'POST',
            headers: this.state.headers,
            body: JSON.stringify({
                name: this.state.playlist_name,
                description: this.state.playlist_description,
                public: this.state.playlist_public
            })
        })
        .then( response => response.json())
        .then( data => {
            console.log("Data: ", data)
            this.closeModal()
            this.setState({
                playlist_name: 'New Playlist',
                playlist_description: 'Give you playlist a catchy description.',
                playlist_public: false
            })
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    handleName(e){
        this.setState({
            playlist_name: e.target.value
        })
    }

    handleDescription(e){
        this.setState({
            playlist_description: e.target.value
        })
    }

    render(){
        return(
            <section className={this.state.visible ? "active modal-container overlay overlay--full" : "modal-container overlay overlay--full"}>
                <div className="modal-content bg-white max-w-680 h-auto p-30 z-index-1 rounded">
                    <div className="modal-header d-flex flex-between w-100 mb-20">
                        <div className="modal-header font-weight-600 uppercase font-12 lh-17">Create playlist</div>
                        <div className="close close--dark h-20-p w-20-p" onClick={() => this.closeModal()}></div>
                    </div>

                    <div className="modal-body">
                        <label className="d-block mb-20" htmlFor="">
                            <span className="d-block font-12 font-weight-500 mb-5">Name</span>
                            <input className="w-100 input box-sizing-border" type="text" placeholder="New Playlist" defaultValue={this.state.playlist_name} onChange={(e) => this.handleName(e)}/>
                        </label>
                        <label className="d-block mb-20" htmlFor="">
                            <span className="d-block font-12 font-weight-500 mb-5">Description</span>
                            <textarea className="textarea" name="" id="" rows="10" placeholder="Give you playlist a catchy description." defaultValue={this.state.playlist_description} onChange={(e) => this.handleDescription(e)}/>
                        </label>
                        <div className="d-flex">
                            <button className="button button--solid mr-20" onClick={() => this.createPlaylist()}>Create</button>
                            <button className="button button--solid" onClick={() => this.closeModal()}>Cancel</button>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}