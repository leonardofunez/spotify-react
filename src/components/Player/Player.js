import React, { Component } from 'react'
import './Player.css'
import { NavLink } from 'react-router-dom'
import { Headers } from '../../config/headers'

import AddPlaylist from '../Modals/AddPlaylist/AddPlaylist'

import store from '../../config/store/store'

export default class Player extends Component{
    constructor() {
        super();

        this.state = {
            headers: Headers,
            current_user_id: '',

            addPlaylist: false,
            
            track_isnew: true,
            track: new Audio(),
            track_src: '',
            track_duration: '0:00',
            track_current_time: '0:00',
            track_playing: false,
            track_title: '',
            track_album_name: '',
            track_album_image: '',
            track_album_id: '',
    
            repeat: 'off',
            durationBar: '0',
            volume: 5,
            mute: false
        }

        this.closeAddPlaylist = this.closeAddPlaylist.bind(this)
    
        store.subscribe(() => {
            this.resetPlayer()
            
            const trackStore =  store.getState().track

            this.setState({
                track_playing: true,
                track_src: trackStore.track_src,
                track_title: trackStore.track_name,
                track_album_id: trackStore.track_album_id,
                track_album_name: trackStore.track_album_name,
                track_album_image: trackStore.track_album_image,
            })

            this.playTrack()
        })
    }

    getTrackTime(duration){
        let s = parseInt(duration % 60)
        if (s < 10) s = '0' + s
        let m = parseInt((duration / 60) % 60)

        return m + ':' + s
    }

    resetPlayer(){
        this.setState({
            track_isnew: true,
            track_duration: '0:00',
            durationBar: '0',
            track_current_time: '0:00',
            track_playing: false,
            track_title: '',
            track_album_name: '',
            track_album_id: '',
        })
    }

    playTrack(){ 
        if( this.state.track_isnew ){
            setTimeout( () => {
                if( this.state.track_src.length > 0 ){
                    this.state.track.src = this.state.track_src
                    this.state.track.play()
                    this.state.track.ontimeupdate = () => {
                        this.setState({
                            track_duration: '0:30',
                            track_current_time: this.getTrackTime(this.state.track.currentTime)
                        })

                        const currentTimeToSeg = parseInt(this.state.track.duration * this.state.track.currentTime / 10) + 10;
                        this.setState({
                            durationBar: currentTimeToSeg
                        })
                    }

                    this.state.track.onended = () => {
                        if( this.state.repeat === 'one'){
                            this.state.track.play()
                        }else{
                            this.resetPlayer()
                        }
                    }
                }else{
                    this.setState({
                        track_playing: true
                    })
                }
            }, 200)
        }else{
            this.state.track.play()
        }
    }

    pauseTrack(){
        this.state.track.pause()
        this.setState({
            track_isnew: false,
            track_playing: false
        })
    }

    repeatTrack(){
        if(this.state.repeat === 'off'){
            this.setState({
                repeat: 'one'
            })
        // }else if(this.state.repeat === 'one'){
        //     this.setState({
        //         repeat: 'all'
        //     })
        }else{
            this.setState({
                repeat: 'off'
            })
        }
    }

    changeCurrentTime(e){
        console.log(e.target.value)
    }

    changeVolume(e){
        this.setState({
            volume: e.target.value
        })
        this.state.track.volume = e.target.value / 10
    }

    mute(){
        this.setState({
            mute: !this.state.mute
        })
        this.state.track.muted = !this.state.mute;
    }

    AddPlaylist(){
        this.setState({
            addPlaylist: true
        })
    }

    closeAddPlaylist(){
        this.setState({
            addPlaylist: false
        })
    }

    render(){
        return(
            <React.Fragment>
                <AddPlaylist visible={this.state.addPlaylist} closeAddPlaylist={this.closeAddPlaylist}></AddPlaylist>
                
                <div className="player-container box-sizing-border fixed d-flex flex-between pos-bottom z-index-999 w-100 bg-blue-darker pt-20 pb-20 pl-60 pr-60 h-100-p">
                    {/* New Playlist */}
                        <div onClick={(e) => this.AddPlaylist()} className="new-playlist c-pointer font-weight-500 font-14 bg-green-neon color-dark pt-10 pr-0 pb-10 pl-45 pr-25 absolute rounded">New Playlist</div>
                    {/* .New Playlist */}

                    {/* Current song */}
                        <div className="current-song d-flex flex-middle w-300-p">
                            <div className="photo-song w-50-p h-50-p mr-10">
                                <NavLink to={'/dashboard/albums/' + this.state.track_album_id}>
                                    <img src={this.state.track_album_image} alt={this.state.track_album_name} className="w-100"/>
                                </NavLink>
                            </div>
                            <div className="info-song">
                                <h2 className="mt-0 mb-5 font-14 truncate">{this.state.track_title}</h2>
                                <NavLink to={'/dashboard/albums/' + this.state.track_album_id}>
                                    <p className="m-0 font-12 truncate">{this.state.track_album_name}</p>
                                </NavLink>
                            </div>
                        </div>
                    {/* .Current song */}

                    {/* Player */}
                        <div className="player max-w-480 absolute-h-center">
                            <div className="controls d-flex flex-center mb-10">
                                <div className="shuffle h-40-p w-40-p c-pointer bg-gray"></div>
                                <div className="prev h-40-p w-40-p c-pointer ml-10 mr-10 bg-gray"></div>
                                
                                <div className="playing h-40-p w-40-p">
                                    { !this.state.track_playing ? 
                                        <div className="play d-block w-100 h-100 c-pointer" onClick={() => this.playTrack()}></div>
                                    :
                                        <div className="pause d-block w-100 h-100 c-pointer" onClick={() => this.pauseTrack()}></div>
                                    }
                                </div>
                                
                                <div className="next h-40-p w-40-p ml-10 mr-10 c-pointer bg-gray"></div>
                                <div className="repeat h-40-p w-40-p c-pointer bg-gray relative" onClick={() => this.repeatTrack()}>
                                    {this.state.repeat === 'one' ?
                                        <span className="repeat_one absolute pos-right italic color-green-neon font-10 font-weight-800 bg-blue-darker d-block circled w-10-p h-10-p align-center">1</span>
                                    : null }

                                    {this.state.repeat === 'all' ?
                                        <span className="repeat_all absolute pos-right italic color-green-neon font-10 font-weight-800 bg-blue-darker d-block circled pl-5 align-center">All</span>
                                    : null}
                                </div>
                            </div>
                            <div className="progress-container d-flex flex-middle">
                                <time className="font-10 mr-10">{this.state.track_duration}</time>
                                <input
                                    type="range" 
                                    id="myRange"
                                    min="1"
                                    max="100"
                                    value={this.state.durationBar}
                                    onChange={(e) => this.changeCurrentTime(e)}
                                    className="slider bg-dark rounded"
                                />
                                <time className="font-10 ml-10">{this.state.track_current_time}</time>
                            </div>
                        </div>
                    {/* .Player */}

                    {/* Volume */}
                        <div className="volume max-w-100 d-flex flex-middle">
                            <div className={this.state.mute ? "speaker mute h-30-p w-30-p mr-10 c-pointer" : "speaker h-30-p w-30-p mr-10 c-pointer"} onClick={() => this.mute()}></div>
                            <input
                                type="range" 
                                id="volume_control"
                                min="1"
                                max="10"
                                value={this.state.volume}
                                onChange={(e) => this.changeVolume(e)}
                                className="slider bg-dark rounded"
                            />
                        </div>
                    {/* .Volume */}
                </div>
            </React.Fragment>
        )
    }
}