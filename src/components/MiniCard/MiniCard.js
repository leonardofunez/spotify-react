import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

export default class MiniCard extends Component {
    render(){
        return(
            <div className={this.props.class + "mini-card c-pointer"}>
                <NavLink to={this.props.photoUrl}>
                    <div className="minicard-image mb-10">
                        <img src={this.props.image} className="w-100" alt={this.props.title}/>
                    </div>
                    <h2 className="minicard-title mt-0 mb-5 font-16">
                        {this.props.title}
                    </h2>
                </NavLink>
                <NavLink to={this.props.subtitleUrl}>
                    <p className="minicard-subtitle m-0 font-12 opacity-6">{this.props.subtitle}</p>
                </NavLink>
            </div>
        )
    }
}