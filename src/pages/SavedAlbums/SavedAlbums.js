import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { Headers } from '../../config/headers'
import MiniCard from '../../components/MiniCard/MiniCard'

import Loader from '../../assets/images/loaders/loading_green_blue.svg'

export default class SavedAlbums extends Component{
    state = {
        headers: Headers,
        loading: true,
        saved_albums: []
    }

    componentDidMount(){
        this.getSavedAlbums()
    }

    componentWillReceiveProps(props){
        this.setState({
            loading: true
        })
    }

    getSavedAlbums(){
        fetch(`https://api.spotify.com/v1/me/albums?limit=20`, {
            method: 'GET',
            headers: this.state.headers
        })
        .then( response => response.json())
        .then( saved_albums  => {
            this.setState({
                saved_albums: saved_albums.items
            })

            setTimeout( () => {
                this.setState({
                    loading: false,
                })
            }, 1000)
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    render(){
        return(
            <div className="top-page w-100 mb-40 z-index-1 relative">
                <div className="album-info">
                    <h1 className="page-title font-weight-600 mt-0 mb-20 font-100">Saved Albums</h1>
                    
                    {this.state.loading ?
                        <div className="d-flex flex-middle relative z-index-1 mb-20">
                            <h2>Loading albums</h2>
                            <img src={Loader} width="30" alt="Loading albums" />
                        </div>
                    :
                        <div className="saved-albums d-flex flex-wrap w-100">
                            {
                                this.state.saved_albums.length ?
                                    this.state.saved_albums.map( (album, index) =>
                                        <MiniCard
                                            key={index}
                                            class="w-18 pl-0 mr-20 mb-40 f-left"
                                            image={album.album.images[0].url}
                                            title={album.album.name}
                                            subtitle={album.album.artists[0].name}
                                            photoUrl={'/dashboard/albums/' + album.album.id}
                                            subtitleUrl={'/dashboard/artists/' + album.album.artists[0].id}
                                        />
                                    )
                                :
                                    <div className="d-inline-block">
                                        <h2>You have no any saved Albums yet :(</h2>
                                        <NavLink to="/dashboard/browse">
                                            <button className="button button--solid c-pointer mr-10">Go to Browse</button>
                                        </NavLink>
                                    </div>
                            }
                        </div>
                    }
                </div>
            </div>
        )
    }
}