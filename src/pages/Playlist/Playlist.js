import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { Headers } from '../../config/headers'

import store from '../../config/store/store'

import DeletePlaylist from '../../components/Modals/DeletePlaylist/DeletePlaylist'
import DeleteTrack from '../../components/Modals/DeleteTrack/DeleteTrack'

import PlayIcon from '../../assets/images/player/play_white_2.svg'
import LikeIcon from '../../assets/images/icons/like_white.svg'
import PlaylistIcon from '../../assets/images/icons/playlist_white.svg'
import RemoveIcon from '../../assets/images/icons/delete_white.svg'
import SpeakerIcon from '../../assets/images/player/speaker_white.svg'

import Loader from '../../assets/images/loaders/loading_green_blue.svg'

export default class Playlist extends Component {
    constructor(props){
        super(props)

        this.closeDeletePlaylist = this.closeDeletePlaylist.bind(this)
        this.closeDeleteTrack = this.closeDeleteTrack.bind(this)
        this.deleteTrack = this.deleteTrack.bind(this)
    }

    state = {
        headers: Headers,
        loading: true,

        playlist_id: this.props.match.params.id,
        playlist_title: '',
        playlist_photo: '',
        tracks: [],
        track_active: '',
        track_list_filter: [],
        track_list_filter_value: '',
        
        playlists: [],
        track_id_to_playlist: '',
        
        deletePlaylist: false,
        deleteTrack: false,
        track_to_delete: []
    }

    componentDidMount(){
        this.getPlaylistInfo()
        this.getPlaylistTracks()
    }

    componentWillReceiveProps(props){
        this.setState({
            loading: true
        })
        
        if(this.state.playlist_id !== props.match.params.id){
            this.setState({playlist_id: props.match.params.id}, () =>{
                this.getPlaylistInfo()
                this.getPlaylistTracks()
            })
        }
    }

    // Loading Playlist Info
        getLikedTracks(){
            fetch(`https://api.spotify.com/v1/me/tracks`, { headers: this.state.headers })
            .then( response => response.json() )
            .then( response =>{
                return response
            })
            .catch( error => 
                console.error('Error:', error)
            )
        }

        getPlaylistInfo(){
            fetch(`https://api.spotify.com/v1/playlists/${this.state.playlist_id}`, { headers: this.state.headers })
            .then( response => response.json())
            .then( playlist  => {
                this.setState({
                    playlist_title: playlist.name,
                    playlist_photo: playlist.images.length > 0 ? playlist.images[0].url : ''
                })
            })
            .catch( error => 
                console.error('Error:', error)
            );
        }

        getPlaylistTracks(){
            fetch(`https://api.spotify.com/v1/playlists/${this.state.playlist_id}/tracks`, { headers: this.state.headers })
            .then( response => response.json())
            .then( tracks  => {
                this.setState({
                    tracks: []
                })
                this.createTrackList(tracks.items)
                this.getAllPlaylist()
            })
            .catch( error => 
                console.error('Error:', error)
            );
        }

        createTrackList(tracks){
            tracks.map( track => {
                this.setState({
                    tracks: [...this.state.tracks, {
                        id: track.track.id,
                        name: track.track.name,
                        preview_url: track.track.preview_url,
                        duration_ms: track.track.duration_ms,
                        artist_id: track.track.artists[0].id,
                        artist_name: track.track.artists[0].name,
                        album_id: track.track.album.id,
                        album_name: track.track.album.name,
                        album_images: track.track.album.images,
                        liked: false
                    }],
                    track_list_filter: this.state.tracks
                })
            })

            setTimeout( () => {
                this.setState({
                    loading: false,
                })
            }, 1000)
        }
    // .Loading Playlist Info

    getAllPlaylist(){
        fetch("https://api.spotify.com/v1/me/playlists", { headers: this.state.headers })
        .then( response => response.json())
        .then( playlists  => {
            this.setState({ playlists: playlists.items})
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    getTrackTime(duration){
        let minutes = Math.floor(duration / 60000)
        let seconds = ((duration % 60000) / 1000).toFixed(0)
        return minutes + ":" + (seconds < 10 ? '0' : '') + seconds
    }

    playTrack(track, index){
        const track_info = {
            track_name: track.name,
            track_src: track.preview_url,
            track_album_id: track.album_id,
            track_album_name: track.album_name,
            track_album_image: track.album_images[2].url
        }
        store.dispatch({
            type: "PLAY_TRACK",
            track: track_info
        })
    }

    likeTrack(track_id){
        fetch(`https://api.spotify.com/v1/me/tracks?ids=${track_id}`, {
            method: 'PUT',
            headers: this.state.headers
        })
        .then( response =>  {
            console.log(response)
        })
        .catch( error => 
            console.error('Error:', error)
        )
    }

    dislikeTrack(track_id){
        fetch(`https://api.spotify.com/v1/me/tracks?ids=${track_id}`, {
            method: 'DELETE',
            headers: this.state.headers
        })
        .then( response =>  {
            console.log(response)
        })
        .catch( error => 
            console.error('Error:', error)
        )
    }

    sendToPlaylist(track_id, playlist_id){
        fetch(`https://api.spotify.com/v1/playlists/${playlist_id}/tracks?uris=spotify:track:${track_id}`,{
            method: 'POST',
            headers: this.state.headers,
        })
        .then( response => response.json())
        .then( response => {
            this.handleHidePlaylistToSendtrack()
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    deleteTrack(){
        const trackId = this.state.track_to_delete.track_id
        fetch(`https://api.spotify.com/v1/playlists/${this.state.playlist_id}/tracks?uri`, {
            method: 'DELETE',
            headers: this.state.headers,
            body: JSON.stringify({
                "tracks": [
                    { "uri": `spotify:track:${trackId}`}
                ]
            })
        })
        .then( response => response.json() )
        .then( response => {
            this.setState({
                // tracks: [
                //     ...this.state.tracks.slice(0, track_index),
                //     ...this.state.tracks.slice(track_index + 1, this.state.tracks.length)
                // ]
                tracks: this.state.tracks.filter( item => item.id !== trackId )
            })
            this.closeDeleteTrack()
        })
    }

    // Modal to delete track
        openDeleteTrack(track_id, index){
            this.setState({
                deleteTrack: true,
                track_to_delete: {
                    track_id: track_id,
                    track_index: index
                }
            })
        }

        closeDeleteTrack(){
            this.setState({
                deleteTrack: false
            })
        }
    // .Modal to delete track

    // Modal to delete playlist
        openDeletePlaylist(){
            this.setState({
                deletePlaylist: true
            })
        }

        closeDeletePlaylist(){
            this.setState({
                deletePlaylist: false
            })
        }
    // .Modal to delete playlist

    trackListFilter(e){
        const inputValue = e.target.value  
        const tracks = this.state.tracks.filter( track => {
            let trackName = track.name.toLowerCase()
            return trackName.indexOf( inputValue.toLowerCase() ) !== -1
        })
        this.setState({
            track_list_filter: tracks,
            track_list_filter_value: inputValue
        })
    }

    // Handlers
        handleActivateTrack(index){
            this.setState({ track_active : index })
        }

        handleShowPlaylistToSendtrack(track_id){
            this.setState({ track_id_to_playlist : track_id })
        }

        handleHidePlaylistToSendtrack(){
            this.setState({ track_id_to_playlist : '' })
        }
    // Handlers

    render(){
        const stylePlaylist = {
            background: 'url(' + this.state.playlist_photo + ') no-repeat center 10% / cover'
        }

        return(
            <React.Fragment>
                <DeletePlaylist visible={this.state.deletePlaylist} closeDeletePlaylist={this.closeDeletePlaylist}></DeletePlaylist>
                <DeleteTrack visible={this.state.deleteTrack} closeDeleteTrack={this.closeDeleteTrack} deleteTrack={this.deleteTrack}></DeleteTrack>

                <div className="playlist-image w-100 h-400-p absolute pos-left pos-top opacity-2" style={stylePlaylist}>
                    <div className="overlay-bottom"></div>
                </div>

                <div className="top-page w-100 mb-40 z-index-1 relative">
                    <div className="album-info">
                        <span className="font-12 font-weight-500 d-block mb-5">PLAYLIST</span>
                        <h1 className="page-title font-weight-600 mt-0 mb-20 font-100 lh-1">{this.state.playlist_title}</h1>

                        <div className="d-flex">
                            {this.state.tracks.length ?
                                <button className="button button--solid c-pointer mr-10">Play</button>
                            :
                                null
                            }
                            <button className="button c-pointer relative button--semi-circled w-30-p h-30-p mr-10">
                                <i className="edit-icon--green-neon d-block w-100 h-100 absolute pos-left pos-top"></i>
                            </button>
                            <button onClick={(e) => this.openDeletePlaylist()} className="button c-pointer relative button--semi-circled button--border-red w-30-p h-30-p mr-10">
                                <i className="remove-icon--green-neon d-block w-100 h-100 absolute pos-left pos-top"></i>
                            </button>
                        </div>
                    </div>
                </div>
                
                {this.state.loading ?
                    <div className="d-flex flex-middle relative z-index-1 mb-20">
                        <h2>Loading tracks</h2>
                        <img src={Loader} width="30" alt="Loading tracks..." />
                    </div>
                :
                    this.state.track_list_filter.length ?
                        <div className="playlist-tracks relative z-index-2">
                            <div className="d-flex max-w-300 mb-20">
                                <input className="input input--bg-dark" type="search" placeholder="Filter" value={this.state.track_list_filter_value} onChange={this.trackListFilter.bind(this)}/>
                            </div>
                            <table className="table w-100" cellSpacing="0" cellPadding="0">
                                <thead>
                                    <tr>
                                        <td className="p-10 w-10-p"></td>
                                        <td className="pt-10 pr-20 pb-10 pl-20"><span className="font-weight-500">Title</span></td>
                                        <td className="p-10"><span className="font-weight-500">Artists</span></td>
                                        <td className="p-10"><span className="font-weight-500">Album</span></td>
                                        <td className="p-10 align-center">Actions</td>
                                        <td className="p-10 align-right w-80-p"><span className="font-weight-500">Duration</span></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.track_list_filter.map( (track, index) =>
                                        <tr key={index} className={[!track.preview_url ? "no-preview-url " : null , this.state.track_active === index ? " active track-row" : " track-row"]}>
                                            <td className="bg-dark w-10-p pr-15">
                                                {track.preview_url ? 
                                                    <div className="track-play d-flex flex-around h-30-p w-100 c-pointer">
                                                        <img className="play-icon" width="20" src={PlayIcon} alt="Play" onClick={() => {this.playTrack(track); this.handleActivateTrack(index)}} />
                                                        <img className="pause-icon" width="18" src={SpeakerIcon} alt="Pause" />
                                                    </div>
                                                :
                                                    null
                                                }
                                            </td>
                                            <td className="pt-10 pr-20 pb-10 pl-20 track-name">
                                                <span className="font-14 m-0 truncate">{track.name}</span>
                                            </td>
                                            <td className="p-10">
                                                <NavLink to={'/dashboard/artists/' + track.artist_id}><span className="font-14 m-0 truncate">{track.artist_name}</span></NavLink>
                                            </td>
                                            <td className="p-10">
                                                <NavLink to={'/dashboard/albums/' + track.album_id} className="truncate"><span className="font-14 m-0 truncate">{track.album_name}</span></NavLink>
                                            </td>
                                            <td className="p-10 track-actions max-w-100 d-flex flex-around">
                                                {track.preview_url ?
                                                    <React.Fragment>
                                                        <div onClick={() => this.likeTrack(track.id)} className="like c-pointer">
                                                            <img src={LikeIcon} alt="Like" width="15"/>
                                                        </div>
                                                        <div onClick={() => this.handleShowPlaylistToSendtrack(track.id)} className="playlist relative c-pointer">
                                                            <img src={PlaylistIcon} alt="Send to playlist" width="15"/>
                                                            
                                                            <div className="send-to-playlist absolute pos-right z-index-1 rounded bg-white align-right pt-10 pb-10" onMouseLeave={() => this.handleHidePlaylistToSendtrack()} data-active={ this.state.track_id_to_playlist === track.id ? "active" : "" }>
                                                                {
                                                                    this.state.playlists.map( (playlist, index) => 
                                                                        <div
                                                                            key={playlist.id}
                                                                            onClick={() => this.sendToPlaylist(track.id, playlist.id)}
                                                                            className="color-dark c-pointer font-13 font-weight-500 truncate p-8 pr-20 pl-20 box-sizing-border"
                                                                        >
                                                                            {playlist.name}
                                                                        </div>
                                                                    )
                                                                }
                                                            </div>
                                                        </div>
                                                        <div onClick={(e) => this.openDeleteTrack(track.id, index)} className="remove c-pointer">
                                                            <img src={RemoveIcon} alt="Remove track" width="16"/>
                                                        </div>
                                                    </React.Fragment>
                                                :
                                                    <React.Fragment>
                                                        <div className="like" alt="Like" width="15">
                                                            <img src={LikeIcon} alt="Like" width="15"/>
                                                        </div>
                                                        <div className="playlist relative">
                                                            <img src={PlaylistIcon} alt="Send to playlist" width="15"/>
                                                        </div>
                                                        <div className="remove">
                                                            <img src={RemoveIcon} alt="Remove track" width="16"/>
                                                        </div>
                                                    </React.Fragment>
                                                }
                                            </td>
                                            <td className="p-10 align-right"><span className="font-14 m-0 truncate">{this.getTrackTime(track.duration_ms)}</span></td>
                                        </tr>
                                    )}
                                </tbody>
                            </table>
                        </div>
                    :
                        <h2 className="relative z-index-2">There are no any tracks here :(</h2>
                    }
            </React.Fragment>
        )
    }
}