import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { Headers } from '../../config/headers'

import store from '../../config/store/store'

import PlayIcon from '../../assets/images/player/play_white_2.svg'
import SpeakerIcon from '../../assets/images/player/speaker_white.svg'
import LikeIcon from '../../assets/images/icons/like_white.svg'
import PlaylistIcon from '../../assets/images/icons/playlist_white.svg'

import Loader from '../../assets/images/loaders/loading_green_blue.svg'

export default class Artist extends Component {
    state = {
        headers: Headers,
        loading: true,

        artist_id: this.props.match.params.id,
        artist_liked: false,
        artist_detail: [],
        artist_name: '',
        artist_photo: '',
        artist_albums: [],
        artist_album_list: [],
        show_album_list: false,
        track_active: '',

        playlists: [],
        track_id_to_playlist: '',

        albums_liked: [],
    }

    componentDidMount(){
        this.getArtistDetail()
        this.getLikedArtists()
        this.getLikedAlbums()
    }

    componentWillReceiveProps(props){
        this.setState({
            loading: true
        })

        if(this.state.artist_id !== props.match.params.id){
            this.setState({artist_id: props.match.params.id}, () =>{
                this.setState({
                    artist_detail: [],
                    artist_albums: [],
                    artist_album_list: []
                })
                this.getArtistDetail()
            })
        }
    }

    likeArtist(){
        if( this.state.artist_liked ){
            fetch(`https://api.spotify.com/v1/me/following?type=artist&ids=${this.state.artist_id}`, 
            {
                headers: this.state.headers,
                method: 'DELETE'
            })
            .then( response => response.text() )
            .then( data => {
                this.setState({
                    artist_liked: false
                })
            })
            .catch( error => 
                console.error('Error:', error)
            );
        }else{
            fetch(`https://api.spotify.com/v1/me/following?type=artist&ids=${this.state.artist_id}`, 
            {
                headers: this.state.headers,
                method: 'PUT'
            })
            .then( response => response.text() )
            .then( data => {
                this.setState({
                    artist_liked: true
                })
            })
            .catch( error => 
                console.error('Error:', error)
            );
        }
    }

    getArtistDetail(){
        fetch(`https://api.spotify.com/v1/artists/${this.state.artist_id}`, { headers: this.state.headers })
        .then( response => response.json())
        .then( artistDetail  => {
            this.setState({
                artist_detail: artistDetail,
                artist_name: artistDetail.name,
                artist_photo: artistDetail.images[0].url,
                artist_followers: artistDetail.followers.total,
            })
            this.getArtistAlbumList()
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    getArtistAlbumList(){
        fetch(`https://api.spotify.com/v1/artists/${this.state.artist_id}/albums`, { headers: this.state.headers })
        .then( response => response.json())
        .then( artistAlbums  => {
            this.setState({ 
                artist_albums: artistAlbums.items
            })
            this.getArtistAlbumsTracks()
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    getArtistAlbumsTracks(){
        this.state.artist_albums.map( (album, index) => {
            fetch(`https://api.spotify.com/v1/albums/${album.id}`, { headers: this.state.headers })
            .then( response => response.json())
            .then( albumDetail  => {
                this.setState({
                    artist_album_list: [...this.state.artist_album_list, albumDetail]
                })
                this.getAllPLaylist()

                setTimeout( () => {
                    this.setState({
                        loading: false,
                    })
                }, 1000)
            })
            .catch( error => 
                console.error('Error:', error)
            )
        })
    }

    getAllPLaylist(){
        fetch("https://api.spotify.com/v1/me/playlists", { headers: this.state.headers })
        .then( response => response.json())
        .then( playlists  => {
            this.setState({ playlists: playlists.items})
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    getLikedArtists(){
        fetch(`https://api.spotify.com/v1/me/following?type=artist`, { headers: this.state.headers })
        .then( response => response.json() )
        .then( response =>{
            if( response.artists.items.filter( artist => artist.id === this.state.artist_id ).length ){
                this.setState({
                    artist_liked: true
                })
            }
        })
        .catch( error => 
            console.error('Error:', error)
        )
    }

    getLikedAlbums(){
        fetch(`	https://api.spotify.com/v1/me/albums`, { headers: this.state.headers })
        .then( response => response.json() )
        .then( albums => {
            this.setState({
                albums_liked: [...this.state.albums_liked, albums.items]
            })
            console.log(this.state.albums_liked)
        })
    }

    changeLikeAlbumState(album_id){
        this.state.albums_liked[0].filter( album => album.album.id === album_id )
        //console.log(album_id)
    }

    getTrackTime(duration){
        let minutes = Math.floor(duration / 60000);
        let seconds = ((duration % 60000) / 1000).toFixed(0);
        return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
    }

    playTrack(track, album_id, album_name, album_image){
        const track_info = {
            track_name: track.name,
            track_src: track.preview_url,
            track_album_id: album_id,
            track_album_name: album_name,
            track_album_image: album_image
        }
        store.dispatch({
            type: "PLAY_TRACK",
            track: track_info
        })
    }

    sendToPlaylist(track_id, playlist_id){
        fetch(`https://api.spotify.com/v1/playlists/${playlist_id}/tracks?uris=spotify:track:${track_id}`,{
            method: 'POST',
            headers: this.state.headers,
        })
        .then( response => response.json())
        .then( response => {
            this.handleHidePlaylistToSendtrack()
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    changeDateFormat(date){
        const ISODate = new Date(date)
        return ISODate.toDateString()
    }

    // Handlers
        handleActivateTrack(track_id){
            this.setState({ track_active : track_id })
        }

        handleShowPlaylistToSendtrack(track_id){
            this.setState({ track_id_to_playlist : track_id })
        }

        handleHidePlaylistToSendtrack(){
            this.setState({ track_id_to_playlist : '' })
        }
    // .Handlers

    render(){
        const styleArtist = {
            background: 'url(' + this.state.artist_photo + ') no-repeat center / cover'
        }

        return(
            <React.Fragment>
                <div className="artists-image w-100 h-400-p absolute pos-left pos-top opacity-4" style={styleArtist}>
                    <div className="overlay-bottom"></div>
                </div>

                <div className="top-page w-100 mb-40 z-index-1 relative">
                    <div className="album-info">
                        <span className="font-12 font-weight-500 d-block mb-5">ARTIST</span>
                        <h1 className="page-title font-weight-600 mt-0 mb-20 font-100 lh-1">{this.state.artist_name}</h1>

                        <div className="d-flex">
                            <button className="button button--solid c-pointer mr-10">Play</button>
                            <button onClick={() => this.likeArtist()} className={ this.state.artist_liked ? "button button--liked c-pointer relative button--semi-circled w-30-p h-30-p mr-10" : "button c-pointer relative button--semi-circled w-30-p h-30-p mr-10"}>
                                <i className={ this.state.artist_liked ? "like-icon like-icon--green-neon active d-block w-100 h-100 absolute pos-left pos-top" : "like-icon like-icon--green-neon d-block w-100 h-100 absolute pos-left pos-top"}></i>
                            </button>
                        </div>
                    </div>
                </div>

                {/* Album List */}
                    {this.state.loading ?
                        <div className="d-flex flex-middle relative z-index-1 mb-20">
                            <h2>Loading albums and tracks</h2>
                            <img src={Loader} width="30" alt="Loading albums"/>
                        </div>
                    :
                        <div className="albumlist relative z-index-1">
                            <h2 className="border-b border-blue-dark pb-20 mb-20">Albums</h2>
                            {this.state.artist_album_list.map( (album, index) =>
                                <section className="mb-60 border-b border-blue-dark" key={index}>
                                    <div className="album d-flex mb-40">
                                        <div className="album-image w-170-p mr-20">
                                            <NavLink to={`/dashboard/albums/${album.id}`}>
                                                <img className="w-100" src={album.images[1].url} alt={album.name} />
                                            </NavLink>
                                        </div>
                                        <div className="album-info">
                                            <span className="d-block mb-10 font-12">{this.changeDateFormat(album.release_date)}</span>
                                            <h2 className="font-18 font-weight-600 mt-0 mb-20">
                                                <NavLink to={`/dashboard/albums/${album.id}`}>{album.name}</NavLink>
                                            </h2>
                                            {/* {
                                                this.changeLikeAlbumState(album.id) ? <p>YES</p> : <p>NO</p>
                                            } */}
                                            <button className="button c-pointer relative button--semi-circled w-30-p h-30-p mr-10">
                                                <i className="like-icon--green-neon d-block w-100 h-100 absolute pos-left pos-top"></i>
                                            </button>
                                            {/* <button className="button c-pointer relative button--semi-circled w-30-p h-30-p"><i className="more-icon--green-neon d-block w-100 h-100 absolute pos-left pos-top"></i></button> */}
                                        </div>
                                    </div>

                                    <div className="playlist-tracks w-100 mb-40">
                                        <table className="table w-100" cellSpacing="0" cellPadding="0">
                                            <thead>
                                                <tr>
                                                    <td className="p-10 w-10-p pr-15"></td>
                                                    <td className="p-10 w-40-p align-center">#</td>
                                                    <td className="pt-10 pr-20 pb-10 pl-20"><span className="font-weight-500">Title</span></td>
                                                    <td className="p-10 align-center w-70-p">Actions</td>
                                                    <td className="p-10 align-right w-70-p"><span className="font-weight-500">Duration</span></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {album.tracks.items.map( (track, index) =>
                                                    <tr key={index} className={[!track.preview_url ? "no-preview-url " : null , this.state.track_active === track.id ? " active track-row" : " track-row"]}>
                                                        <td className={track.id + " bg-dark w-10-p pr-15"}>
                                                            {track.preview_url ? 
                                                                <div className="track-play d-flex flex-around h-30-p w-100 c-pointer">
                                                                    <img className="play-icon" width="20" src={PlayIcon} alt="Play" onClick={() => {this.playTrack(track, album.id, album.name, album.images[2].url); this.handleActivateTrack(track.id)}}/>
                                                                    <img className="pause-icon" width="18" src={SpeakerIcon} alt="Pause" />
                                                                </div>
                                                            :
                                                                null
                                                            }
                                                        </td>
                                                        <td className="p-10 w-40-p align-center">{index + 1}</td>
                                                        <td className="pt-10 pr-20 pb-10 pl-20 track-name"><span className="font-14 m-0 truncate">{track.name}</span></td>
                                                        <td className="pt-10 pr-30 pb-10 pl-10 track-actions max-w-70 d-flex flex-around">
                                                            {track.preview_url ? 
                                                                <React.Fragment>
                                                                    <div className="like"><img src={LikeIcon} alt="Like" width="15"/></div>
                                                                    <div onClick={() => this.handleShowPlaylistToSendtrack(track.id)} className="playlist relative">
                                                                        <img src={PlaylistIcon} alt="Send to playlist" width="15"/>
                                                                        
                                                                        <div className="send-to-playlist absolute pos-right z-index-1 rounded bg-white align-right pt-10 pb-10" onMouseLeave={() => this.handleHidePlaylistToSendtrack()} data-active={ this.state.track_id_to_playlist === track.id ? "active" : "" }>
                                                                            {
                                                                                this.state.playlists.map( (playlist, index) => 
                                                                                    <div
                                                                                        key={playlist.id}
                                                                                        onClick={() => this.sendToPlaylist(track.id, playlist.id)}
                                                                                        className="color-dark c-pointer font-13 font-weight-500 truncate p-8 pr-20 pl-20 box-sizing-border"
                                                                                    >
                                                                                        {playlist.name}
                                                                                    </div>
                                                                                )
                                                                            }
                                                                        </div>

                                                                    </div>
                                                                </React.Fragment>
                                                            :
                                                                <React.Fragment>
                                                                    <div className="like" alt="Like" width="15">
                                                                        <img src={LikeIcon} alt="Like" width="15"/>
                                                                    </div>
                                                                    <div className="playlist relative">
                                                                        <img src={PlaylistIcon} alt="Send to playlist" width="15"/>
                                                                    </div>
                                                                </React.Fragment>
                                                            }
                                                        </td>
                                                        <td className="p-10 align-right font-14">{this.getTrackTime(track.duration_ms)}</td>
                                                    </tr>
                                                )}
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            )}
                        </div>
                    }
                {/* .Album List */}
            </React.Fragment>
        )
    }
}