import React, { Component } from 'react'
import { Headers } from '../../config/headers'
import MiniCard from '../../components/MiniCard/MiniCard'

import Loader from '../../assets/images/loaders/loading_green_blue.svg'

export default class Browse extends Component {
    constructor(props){
        super(props)
        this.handleActivateMenu = this.handleActivateMenu.bind(this);
    }

    state = {
        headers: Headers,
        loading: true,
        menu_active: 0,
        menu: [
            'New Releases',
            'Featured Playlists',
            'Overview',
            'Podcasts',
            'Charts',
            'Genres & Moods',
            'Discover',
            'Concerts'
        ],
        browse_type: 'new_releases',
        browse_content: [],

        new_releases: [],
        featured_playlists: [],
    }

    componentDidMount(){
        this.getNewReleases();
    }

    componentWillReceiveProps(props){
        this.setState({
            loading: true
        })
    }

    resetContent(){
        this.setState({
            browse_content: []
        })
        
        setTimeout( () => {
            this.setState({
                loading: false,
            })
        }, 1000)
    }
    
    getNewReleases(){
        this.resetContent()
        fetch("https://api.spotify.com/v1/browse/new-releases", { headers: this.state.headers })
        .then( response => response.json())
        .then( new_releases  => {
            this.setState({ 
                browse_type: 'new_releases',
                browse_content: new_releases.albums.items
            })
        })
        .catch( error => 
            console.log('Error:', error)
        );
    }

    getFeaturedPlaylists(){
        this.resetContent()
        fetch("https://api.spotify.com/v1/browse/featured-playlists", { headers: this.state.headers })
        .then( response => response.json())
        .then( featured_playlists  => {
            this.setState({
                browse_type: 'featured_playlists',
                browse_content: featured_playlists.playlists.items
            })
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    handleActivateMenu(index){
        this.setState({ menu_active : index })
    }

    render(){
        return(
            <React.Fragment>
                <section className="browse-page">
                    {/* Top Page  */}
                        <div className="top-page w-100 mb-40">
                            <h1 className="page-title font-weight-600 mt-0 mb-20">Browse</h1>
                            <nav className="nav-page-header d-flex">
                                {/* {this.state.menu.map( (item, index) => 
                                    <div key={index + '-' + item} onClick={(e) => this.handleActivateMenu(index)} className={this.state.menu_active === index ? 'nav-item font-weight-500 mr-30 pb-10 relative c-pointer active' : 'nav-item font-weight-500 mr-30 pb-10 relative c-pointer'}>
                                        <span>{item}</span>
                                        <span className="bg-green-neon nav-item-line h-1-p d-block absolute-h-center pos-bottom"></span>
                                    </div>
                                )} */}
                                <div onClick={(e) => {this.handleActivateMenu(0); this.getNewReleases();}} className={this.state.menu_active === 0 ? 'nav-item font-weight-500 mr-30 pb-10 relative c-pointer active' : 'nav-item font-weight-500 mr-30 pb-10 relative c-pointer'}>
                                    <span>New Releases</span>
                                    <span className="bg-green-neon nav-item-line h-1-p d-block absolute-h-center pos-bottom"></span>
                                </div>
                                <div onClick={(e) => {this.handleActivateMenu(1); this.getFeaturedPlaylists();}} className={this.state.menu_active === 1 ? 'nav-item font-weight-500 mr-30 pb-10 relative c-pointer active' : 'nav-item font-weight-500 mr-30 pb-10 relative c-pointer'}>
                                    <span>Featured Playlists</span>
                                    <span className="bg-green-neon nav-item-line h-1-p d-block absolute-h-center pos-bottom"></span>
                                </div>
                            </nav>
                        </div>
                    {/* .Top Page */}
                    
                    {this.state.loading ?
                        <div className="d-flex flex-middle relative z-index-1 mb-20">
                            <h2>Loading albums</h2>
                            <img src={Loader} width="30" alt="Loading albums" />
                        </div>
                    :
                    
                        <div className="categories d-flex flex-wrap w-100">
                            { this.state.browse_content.map( (album, index) => 
                                this.state.browse_type === 'new_releases' ?
                                    <MiniCard
                                        key={index}
                                        class="w-18 pl-0 mr-20 mb-40 f-left"
                                        image={album.images[0].url}
                                        title={album.name}
                                        subtitle={album.artists[0].name}
                                        photoUrl={'/dashboard/albums/' + album.id}
                                        subtitleUrl={'/dashboard/artists/' + album.artists[0].id}
                                    />
                                :
                                    <MiniCard
                                        key={index}
                                        class="w-18 pl-0 mr-20 mb-40 f-left"
                                        image={album.images[0].url}
                                        title={album.name}
                                        subtitle={`Tracks: ${album.tracks.total}`}
                                        photoUrl={'/dashboard/playlists/' + album.id}
                                        subtitleUrl={'/dashboard/playlists/' + album.id}
                                    />
                            )}
                        </div>
                    }
                </section>
            </React.Fragment>
        )
    }
}