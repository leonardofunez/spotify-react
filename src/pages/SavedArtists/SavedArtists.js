import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { Headers } from '../../config/headers'
import MiniCard from '../../components/MiniCard/MiniCard'

import Loader from '../../assets/images/loaders/loading_green_blue.svg'

export default class SavedArtists extends Component{
    state = {
        headers: Headers,
        loading: true,
        saved_artists: []
    }

    componentDidMount(){
        this.getSavedArtists()
    }

    componentWillReceiveProps(props){
        this.setState({
            loading: true
        })
    }

    getSavedArtists(){
        fetch(`https://api.spotify.com/v1/me/following?type=artist`, { headers: this.state.headers })
        .then( response => response.json())
        .then( saved_artists  => {
            this.setState({
                saved_artists: saved_artists.artists.items
            })

            setTimeout( () => {
                this.setState({
                    loading: false,
                })
            }, 1000)
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    render(){
        return(
            <div className="top-page w-100 mb-40 z-index-1 relative">
                <div className="album-info">
                    <h1 className="page-title font-weight-600 mt-0 mb-20 font-100">Saved Artists</h1>
                    
                    {this.state.loading ?
                        <div className="d-flex flex-middle relative z-index-1 mb-20">
                            <h2>Loading artists</h2>
                            <img src={Loader} width="30" alt="loading artists" />
                        </div>
                    :
                        <div className="saved-artists d-flex flex-wrap w-100">
                            {
                                this.state.saved_artists.length ?
                                    this.state.saved_artists.map( (artist, index) =>
                                        <MiniCard
                                            key={index}
                                            class="w-18 pl-0 mr-20 mb-40 f-left"
                                            image={artist.images[0].url}
                                            title={artist.name}
                                            subtitle={artist.genres[0]}
                                            photoUrl={'/dashboard/artists/' + artist.id}
                                            subtitleUrl={'/dashboard/artists/' + artist.id}
                                        />
                                    )
                                :
                                    <div className="d-inline-block">
                                        <h2>You have no any saved Artists yet :(</h2>
                                        <NavLink to="/dashboard/browse">
                                            <button className="button button--solid c-pointer mr-10">Go to Browse</button>
                                        </NavLink>
                                    </div>
                            }
                        </div>
                    }
                </div>
            </div>
        )
    }
}