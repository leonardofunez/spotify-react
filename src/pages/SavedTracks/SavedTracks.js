import React, { Component } from 'react'
import {Headers} from '../../config/headers'

import Loader from '../../assets/images/loaders/loading_green_blue.svg'

export default class SavedTracks extends Component{
    state = {
        headers: Headers,
        loading: true,
        saved_tracks: []
    }

    componentDidMount(){
        this.getSavedTracks()
    }

    componentWillReceiveProps(props){
        this.setState({
            loading: true
        })
    }

    getTrackTime(duration){
        let minutes = Math.floor(duration / 60000);
        let seconds = ((duration % 60000) / 1000).toFixed(0);
        return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
    }

    getSavedTracks(){
        fetch(`https://api.spotify.com/v1/me/tracks`, { headers: this.state.headers })
        .then( response => response.json())
        .then( savedTracks  => {
            this.setState({
                saved_tracks: savedTracks.items
            })

            setTimeout( () => {
                this.setState({
                    loading: false,
                })
            }, 1000)
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    render(){
        return(
            <React.Fragment>
                <div className="top-page w-100 mb-40 z-index-1 relative">
                    <div className="album-info">
                        <h1 className="page-title font-weight-600 mt-0 mb-20 font-100">Saved Tracks</h1>

                        <div className="d-flex">
                            <button className="button button--solid c-pointer mr-10">Play</button>
                        </div>
                    </div>
                </div>

                {this.state.loading ?
                    <div className="d-flex flex-middle relative z-index-1 mb-20">
                        <h2>Loading tracks</h2>
                        <img src={Loader} width="30" alt="Loading tracks"/>
                    </div>
                :
                    <table className="table w-100" cellSpacing="0" cellPadding="0">
                        <thead>
                            <tr>
                                <td className="p-10 w-40-p"></td>
                                <td className="p-10 w-40-p align-center">#</td>
                                <td className="pt-10 pr-20 pb-10 pl-20"><span className="font-weight-500">Title</span></td>
                                <td className="p-10 align-right"><span className="font-weight-500">Duration</span></td>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.saved_tracks.map( (item, index) =>
                                <tr className="track c-pointer rounded" key={index}>
                                    <td className="bg-dark w-40-p"><div className="like-icon active h-30-p w-100"></div></td>
                                    <td className="p-10 w-40-p align-center">{index + 1}</td>
                                    <td className="pt-10 pr-20 pb-10 pl-20 track-name"><span className="font-14 m-0 truncate">{item.track.name}</span></td>
                                    <td className="p-10 align-right font-14">{this.getTrackTime(item.track.duration_ms)}</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                }
            </React.Fragment>
        )
    }
}