import React, { Component } from 'react'
import Logo from '../../assets/images/logos/logo_green_neon.svg'
import LoginBG from '../../assets/images/bg/login3.jpeg'
import VectorsBG from '../../assets/images/bg/bg_vectors.svg'

export default class Login extends Component {
    state = {
        user: [],
        clientID: 'b3cde7321c9a469bbe14bd07256dc2fe',
        clientSecret: 'd28fadc17ac946139b37d30ffac9b49e',
        callback_url: 'http://localhost:3000/login',
        app_scopes : `user-read-private
                      user-read-email
                      user-library-read
                      user-library-modify
                      user-follow-read
                      user-follow-modify
                      
                      playlist-read-private
                      playlist-modify-private
                      playlist-modify-public`,
    }

    componentDidMount(){
        const spotifyToken = this.getUrlParam().access_token
        if(spotifyToken){
            localStorage.setItem('spotifyToken', spotifyToken)
            console.log()
            setTimeout( () => {
                window.location.href = '/dashboard'
                //browserHistory.push('/dashboard');
            }, 200)
        }
    }

    getUrlParam(){
        let origin = window.location.origin;
        let hash = window.location.hash.replace('#', '?');
        let url = origin + hash;

        let params = {};
        let parser = document.createElement('a');
        parser.href = url;
        let query = parser.search.substring(1);
        let vars = query.split('&');

        for (let i = 0; i < vars.length; i++) {
            let pair = vars[i].split('=');
            params[pair[0]] = decodeURIComponent(pair[1]);
        }
        return params;
    }

    login = () => {
        //const scope = "user-read-private user-read-email user-library-read playlist-modify-private playlist-read-private playlist-read-collaborative";
        const baseUrl = `https://accounts.spotify.com/authorize/?client_id=${this.state.clientID}&response_type=token&redirect_uri=${encodeURIComponent(this.state.callback_url)}&scope=${encodeURIComponent(this.state.app_scopes)}&state=34fFs29kd09`;
        window.location.href = baseUrl;
    }

    render(){
        const loginBg = {
            background: 'url(' + LoginBG + ') no-repeat center / cover'
        }
        const vectorsBg = {
            background: 'url(' + VectorsBG + ') no-repeat center / cover'
        }

        return(
            <React.Fragment>
                <div className="w-400-p h-400-p pt-110 box-sizing-border bg-blue color-white circled align-center transform-v-center z-index-1">
                    <div>
                        <img src={Logo} alt="Spotify" width="220" className="mb-10"/>
                        <p className="font-14 m-0 mb-5">This app is a React concept using Spotify's API.</p>
                        <p className="font-14 m-0 mb-20">Design and development by <a className="color-green-neon font-weight-500" href="http://leonardofunez.com">Leonardo Funez</a></p>
                        <div className="actions w-100 d-inline-block">
                            <button className="button button--solid d-inline-block mr-10" onClick={this.login}>Sign In</button>
                            <a href="https://www.spotify.com/co/signup/" className="button d-inline-block ml-10" target="_blank" rel="noopener noreferrer">Sign Up</a>
                        </div>
                    </div>
                </div>

                <div className="bg absolute w-100 h-100" style={loginBg}></div>
                <div className="bg absolute w-100 h-100" style={vectorsBg}></div>
            </React.Fragment>
        )
    }
}