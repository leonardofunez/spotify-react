import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { Headers } from '../../config/headers'

import store from '../../config/store/store'

import PlayIcon from '../../assets/images/player/play_white_2.svg'
import LikeIcon from '../../assets/images/icons/like_white.svg'
import PlaylistIcon from '../../assets/images/icons/playlist_white.svg'
import SpeakerIcon from '../../assets/images/player/speaker_white.svg'

import Loader from '../../assets/images/loaders/loading_green_blue.svg'

export default class Album extends Component {
    state = {
        headers: Headers,
        loading: true,

        album_id: this.props.match.params.id,
        album_liked: false,
        album_author: '',
        album_author_id: '',
        album_image: 0,
        album_detail: [],
        album_tracks: [],
        track_active: '',

        playlists: [],
        track_id_to_playlist: '',
    }

    componentDidMount(){
        this.getAlbumDetail()
        this.getLikedAlbums()
    }

    componentWillReceiveProps(props){
        this.setState({
            loading: true
        })
    }

    getAlbumDetail(){
        fetch(`https://api.spotify.com/v1/albums/${this.state.album_id}`, { headers: this.state.headers })
        .then( response => response.json())
        .then( albumDetail  => {
            this.setState({ 
                album_detail: albumDetail,
                album_author: albumDetail.artists[0].name,
                album_author_id: albumDetail.artists[0].id,
                album_image: albumDetail.images[0].url,
                album_tracks: albumDetail.tracks.items
            })
            this.getAllPLaylist()

            setTimeout( () => {
                this.setState({
                    loading: false,
                })
            }, 1000)
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    getLikedAlbums(){
        fetch(`https://api.spotify.com/v1/me/albums`, { headers: this.state.headers })
        .then( response => response.json() )
        .then( likedAlbums => {
            if( likedAlbums.items.filter( album => album.album.id === this.state.album_id ).length ){
                this.setState({
                    album_liked: true
                })
            }
        })
        .catch( error => 
            console.error('Error:', error)
        )
    }

    likeAlbum(){
        if( this.state.album_liked ){
            fetch(`https://api.spotify.com/v1/me/albums?ids=${this.state.album_id}`, {
                method: 'DELETE',
                headers: this.state.headers
            })
            .then( response => response.text() )
            .then( response =>{
                this.setState({
                    album_liked: false
                })
            })
            .catch( error =>
                console.log("Error: ", error)
            )
        }else{
            fetch(`https://api.spotify.com/v1/me/albums?ids=${this.state.album_id}`, {
                method: 'PUT',
                headers: this.state.headers
            })
            .then( response => response.text() )
            .then( response =>{
                this.setState({
                    album_liked: true
                })
            })
            .catch( error =>
                console.log("Error: ", error)
            )
        }
    }

    getAllPLaylist(){
        fetch("https://api.spotify.com/v1/me/playlists", { headers: this.state.headers })
        .then( response => response.json())
        .then( playlists  => {
            this.setState({ playlists: playlists.items})
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    getTrackTime(duration){
        let minutes = Math.floor(duration / 60000);
        let seconds = ((duration % 60000) / 1000).toFixed(0);
        return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
    }

    playTrack(track){
        const track_info = {
            track_name: track.name,
            track_src: track.preview_url,
            track_album_id: this.state.album_detail.id,
            track_album_name: this.state.album_detail.name,
            track_album_image: this.state.album_detail.images[2].url
        }
        store.dispatch({
            type: "PLAY_TRACK",
            track: track_info
        })
    }

    sendToPlaylist(track_id, playlist_id){
        fetch(`https://api.spotify.com/v1/playlists/${playlist_id}/tracks?uris=spotify:track:${track_id}`,{
            method: 'POST',
            headers: this.state.headers,
        })
        .then( response => response.json())
        .then( response => {
            this.handleHidePlaylistToSendtrack()
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    // Handlers
        handleActivateTrack(index){
            this.setState({ track_active : index })
        }

        handleShowPlaylistToSendtrack(track_id){
            this.setState({ track_id_to_playlist : track_id })
        }

        handleHidePlaylistToSendtrack(){
            this.setState({ track_id_to_playlist : '' })
        }
    // .Handlers

    render(){
        return(
            <React.Fragment>
                <div className="top-page w-100 mb-40 d-flex">
                    <div className="album-image w-170-p">
                        <img className="w-100" src={this.state.album_image} alt={this.state.album_detail.name} />
                    </div>
                    <div className="album-info pl-20">
                        <span className="font-12 font-weight-500 d-block mb-5">ALBUM</span>
                        <h1 className="page-title font-weight-600 mt-0 mb-20">{this.state.album_detail.name}</h1>
                        <p className="page-subtitle mt-0 mb-5 font-14"><span className="opacity-6">By</span> <NavLink to={`/dashboard/artists/${this.state.album_author_id}`}>{this.state.album_author}</NavLink></p>
                        <p className="page-subtitle mt-0 mb-25 font-14"><span className="opacity-6">Total tracks: </span>{this.state.album_detail.total_tracks}</p>

                        <div className="d-flex">
                            <button className="button button--solid c-pointer mr-10">Play</button>
                            <button className={ this.state.album_liked ? "button button--liked c-pointer relative button--semi-circled w-30-p h-30-p mr-10" : "button c-pointer relative button--semi-circled w-30-p h-30-p mr-10"} onClick={() => this.likeAlbum()}>
                                <i className={ this.state.album_liked ? "like-icon like-icon--green-neon active d-block w-100 h-100 absolute pos-left pos-top" : "like-icon--green-neon d-block w-100 h-100 absolute pos-left pos-top" }></i>
                            </button>
                            {/* <button className="button c-pointer relative button--circled w-30-p h-30-p"><i className="more-icon--green-neon d-block w-100 h-100 absolute pos-left pos-top"></i></button> */}
                        </div>
                    </div>
                </div>
                
                {this.state.loading ?
                    <div className="d-flex flex-middle relative z-index-1 mb-20">
                        <h2>Loading tracks</h2>
                        <img src={Loader} width="30" alt="Loading tracks" />
                    </div>
                :
                    <div className="playlist-tracks">
                        <table className="table w-100" cellSpacing="0" cellPadding="0">
                            <thead>
                                <tr>
                                    <td className="p-10 w-10-p pr-15"></td>
                                    <td className="p-10 w-40-p align-center">#</td>
                                    <td className="pt-10 pr-20 pb-10 pl-20"><span className="font-weight-500">Title</span></td>
                                    <td className="p-10 w-70-p">Actions</td>
                                    <td className="p-10 align-right w-70-p"><span className="font-weight-500">Duration</span></td>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.album_tracks.map( (track, index) =>
                                    <tr key={index} className={[!track.preview_url ? "no-preview-url " : null , this.state.track_active === index ? " active track-row" : " track-row"]}>
                                        <td className="bg-dark w-10-p pr-15">
                                            {track.preview_url ? 
                                                <div className="track-play d-flex flex-around h-30-p w-100 c-pointer">
                                                    <img className="play-icon" width="20" src={PlayIcon} alt="Play" onClick={() => {this.playTrack(track); this.handleActivateTrack(index)}} />
                                                    <img className="pause-icon" width="18" src={SpeakerIcon} alt="Playing" />
                                                </div>
                                            :
                                                null
                                            }
                                        </td>
                                        <td className="p-10 w-40-p align-center">{index + 1}</td>
                                        <td className="pt-10 pr-20 pb-10 pl-20 track-name">
                                            <span className="font-14 m-0 truncate">{track.name}</span>
                                        </td>
                                        <td className="p-10 track-actions d-flex flex-around">
                                            {track.preview_url ? 
                                                <React.Fragment>
                                                    <div className="like c-pointer ">
                                                        <img src={LikeIcon} alt="Like" width="15"/>
                                                    </div>
                                                    <div onClick={() => this.handleShowPlaylistToSendtrack(track.id)} className="playlist relative c-pointer">
                                                        <img src={PlaylistIcon} alt="Send to playlist" width="15"/>

                                                        <div className="send-to-playlist absolute pos-right z-index-1 rounded bg-white align-right pt-10 pb-10" onMouseLeave={() => this.handleHidePlaylistToSendtrack()} data-active={ this.state.track_id_to_playlist === track.id ? "active" : "" }>
                                                            {
                                                                this.state.playlists.map( (playlist, index) => 
                                                                    <div
                                                                        key={playlist.id}
                                                                        onClick={() => this.sendToPlaylist(track.id, playlist.id)}
                                                                        className="color-dark c-pointer font-13 font-weight-500 truncate p-8 pr-20 pl-20 box-sizing-border"
                                                                    >
                                                                        {playlist.name}
                                                                    </div>
                                                                )
                                                            }
                                                        </div>
                                                    </div>
                                                </React.Fragment>
                                            :
                                                <React.Fragment>
                                                    <div className="like" alt="Like" width="15">
                                                        <img src={LikeIcon} alt="Like" width="15"/>
                                                    </div>
                                                    <div className="playlist relative">
                                                        <img src={PlaylistIcon} alt="Send to playlist" width="15"/>
                                                    </div>
                                                </React.Fragment>
                                            }
                                        </td>
                                        <td className="p-10 align-right font-14">{this.getTrackTime(track.duration_ms)}</td>
                                    </tr>
                                )}
                            </tbody>
                        </table>
                    </div>
                }
            </React.Fragment>
        )
    }
}