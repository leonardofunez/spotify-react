import React, { Component } from 'react'
import MiniCard from '../../components/MiniCard/MiniCard'
import { Headers } from '../../config/headers'

import NoImage from '../../assets/images/bg/no_image.svg'
import Loader from '../../assets/images/loaders/loading_green_blue.svg'

export default class User extends Component{
    state = {
        headers: Headers,
        loading: true,
        user_id: this.props.match.params.id,
        user_name: '',
        user_photo: '',
        user_playlists: []
    }

    componentDidMount(){
        this.getUser()
        this.getPLaylist()
    }

    componentWillReceiveProps(props){
        this.setState({
            loading: true
        })
    }

    getUser(){
        fetch(`https://api.spotify.com/v1/users/${this.state.user_id}`, { headers: this.state.headers })
        .then( response => response.json())
        .then( user  => {
            this.setState({
                user_name: user.id,
                user_photo: user.images[0].url
            })
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    getPLaylist(){
        fetch(`https://api.spotify.com/v1/users/${this.state.user_id}/playlists`, { headers: this.state.headers })
        .then( response => response.json())
        .then( playlists  => {
            this.setState({ user_playlists: playlists.items})

            setTimeout( () => {
                this.setState({
                    loading: false,
                })
            }, 1000)
        })
        .catch( error => 
            console.error('Error:', error)
        );
    }

    render(){
        const styleUser = {
            background: 'url(' + this.state.user_photo + ') no-repeat center 10% / cover'
        }
        return(
            <React.Fragment>
                <div className="top-page w-100 mb-40 d-flex">
                    <div className="album-image w-100-p h-100-p circled overflow-hidden" style={styleUser}></div>
                    <div className="album-info pl-20">
                        <span className="font-12 font-weight-500 d-block mb-5">USER</span>
                        <h1 className="page-title font-weight-600 mt-0 mb-20">{this.state.user_name}</h1>
                        <div className="d-flex">
                            <button className="button c-pointer relative button--circled w-30-p h-30-p"><i className="more-icon--green-neon d-block w-100 h-100 absolute pos-left pos-top"></i></button>
                        </div>
                    </div>
                </div>

                {/* Playlist */}
                    {this.state.loading ?
                        <div className="d-flex flex-middle relative z-index-1 mb-20">
                            <h2>Loading my playlists</h2>
                            <img src={Loader} width="30" alt="Loading playlists" />
                        </div>
                    :
                        <div className="public-playlist">
                            <h2 className="font-18">Public playlists</h2>
                            <div className="categories d-flex flex-wrap w-100">
                                { this.state.user_playlists.map( (album, index) => 
                                    <MiniCard
                                        key={index}
                                        class="w-18 pl-0 pr-20 mb-40 f-left"
                                        image={album.images.length ? album.images[0].url : NoImage}
                                        title={album.name}
                                        subtitle={'Tracks: ' + album.tracks.total}
                                        photoUrl={'/playlists/' + album.id}
                                        subtitleUrl={'/playlists/' + album.id}
                                    />
                                )}
                            </div>
                        </div>
                    }
                {/* .Playlist */}
            </React.Fragment>
        )
    }
}